import ballerina/grpc;
import ballerina/io;
import ballerina/lang.'int;

public function main (string... args) {
//
    songsStorageSystemBlockingClient blockingEp = new("http://localhost:9090");

        io:println("WELCOME TO CALI  SONG PLATFORM");
        io:println("******************************");
        io:println("1.write a new record");
        io:println("2.update a record");
        io:println("3.search with key");
        io:println("4.search with key and version");
        io:println("5.search with criteria eg atristname ,song title etc");
        io:println("******************************");

       string heyy = io:readln("Enter choice :");

       if (heyy === "1"){
           //***************WRITING A NEW RECORD **********************
           string name = io:readln("Enter Name :");
           string date  = io:readln("Enter Date  :");
           string band_name  = io:readln("Enter Band Name  :");
           string songs  =  io:readln("Enter Number Of Songs To Be Entered :");
           //******** ENTERING SONGS INTO AN ARRAY************************
           //GETTING USER INPUT FOR MAXIMUM NUMBER OF SONGS 
           int|error song_max_number_to_be_input = 'int:fromString(songs);
           //USING THE SONG[] ARRAY ALREADY CREATED IN THE PROTO TO STORE SONG DETAILS
            Song[] array_of_songs = [];
                if (song_max_number_to_be_input is int) {
            //FOREACH TO ITERATE AND GET USER INPUT BASED ON NUMBER OF SONGS USER WANTS TO INPUT
                    foreach var j in 0...song_max_number_to_be_input {

                        string title = io:readln("Enter title  :");
                        string genre  = io:readln("Enter genre :");
                        string platform  = io:readln("Enter platform :"); 
                        //PUSH THE RESULTS IN THE ARRAY
                        array_of_songs.push({"title":title,"genre":genre,"platform":platform});
                      
                    }  
                } else {
                    io:println("error: oops error ");
                }
             //GETTING USER INPUT FOR MAXIMUM NUMBER OF ARTIST
            string artists  =  io:readln("Enter Number Of artist to be entered  :");
           int|error artist_max_number_to_be_input = 'int:fromString(artists);
             //USING THE Artist[] ARRAY ALREADY CREATED IN THE PROTO TO STORE Artist DETAILS
            Artist[] array_of_artist = [];
                if (artist_max_number_to_be_input is int) {
                   //FOREACH TO ITERATE AND GET USER INPUT BASED ON NUMBER OF Artist USER WANTS TO INPUT     
                    foreach var j in 1...artist_max_number_to_be_input {
                        string Artist_name= io:readln( "Enter name  :");
                        string member  = io:readln("Enter member YES|NO :");
                        //PUSH THE RESULTS IN THE ARRAY
                        if (member==="yes"){
                             array_of_artist.push({"name":Artist_name,"member":MEMBER_YES});
                        }else{
                              array_of_artist.push({"name":Artist_name,"member":MEMBER_NO});
                        }
                       
                      
                    }  
                } else {
                    io:println("error: oops error ");
                }

           var result = blockingEp->insertRec({  "name":name,"date":date,
                                        "band":band_name,"songs":array_of_songs,
                                            "artist":array_of_artist,
                                            "RecordVersion": 1,
                                            "RecordKey":""
                                     });
                //return response 
                    if(result is grpc:Error){
                                io:println(result.reason());
                    }else{
                        io:println(result);
                    }       
       }else if (heyy === "2") {
           //****************UPDATING AN EXISTING RECORD *************************
           string record_key = io:readln("Enter Existing  Key For Record:");
           string name = io:readln("Enter Name :");
           string date  = io:readln("Enter Date  :");
           string band_name  = io:readln("Enter Band Name  :");
           string songs  =  io:readln("Enter Number Of Songs To Be Entered :");
           //******** ENTERING SONGS INTO AN ARRAY************************
           //GETTING USER INPUT FOR MAXIMUM NUMBER OF SONGS 
           int|error song_max_number_to_be_input = 'int:fromString(songs);
           //USING THE SONG[] ARRAY ALREADY CREATED IN THE PROTO TO STORE SONG DETAILS
            Song[] array_of_songs = [];
                if (song_max_number_to_be_input is int) {
            //FOREACH TO ITERATE AND GET USER INPUT BASED ON NUMBER OF SONGS USER WANTS TO INPUT
                    foreach var j in 0...song_max_number_to_be_input {
                        string title = io:readln( "Enter title  :");
                        string genre  = io:readln("Enter genre :");
                        string platform  = io:readln("Enter platform :"); 
                        //PUSH THE RESULTS IN THE ARRAY
                        array_of_songs.push({"title":title,"genre":genre,"platform":platform});
                      
                    }  
                } else {
                    io:println("error: oops error ");
                }
             //GETTING USER INPUT FOR MAXIMUM NUMBER OF ARTIST
            string artists  =  io:readln("Enter Number Of artist to be entered  :");
           int|error artist_max_number_to_be_input = 'int:fromString(songs);
             //USING THE Artist[] ARRAY ALREADY CREATED IN THE PROTO TO STORE Artist DETAILS
            Artist[] array_of_artist = [];
                if (artist_max_number_to_be_input is int) {
                   //FOREACH TO ITERATE AND GET USER INPUT BASED ON NUMBER OF Artist USER WANTS TO INPUT     
                    foreach var j in 0...artist_max_number_to_be_input {
                        string Artist_name= io:readln( "Enter name  :");
                        string member  = io:readln("Enter member YES|NO :");
                        //PUSH THE RESULTS IN THE ARRAY
                        if (member==="yes"){
                             array_of_artist.push({"name":Artist_name,"member":MEMBER_YES});
                        }else{
                              array_of_artist.push({"name":Artist_name,"member":MEMBER_NO});
                        }
                       
                      
                    }  
                } else {
                    io:println("error: oops error ");
                }

           var result = blockingEp->updatingExistingRecord({"RecordKey": record_key,
                                                      "RecordVersion": 1, 
                                                     "the_record": { "name":name,"date":date,
                                        "band":band_name,"songs":array_of_songs,
                                            "artist":array_of_artist,
                                            "RecordVersion": 1,
                                            "RecordKey":""
                                            }
                                     });
                                      //return response 
                                if(result is grpc:Error){
                                            io:println(result.reason());
                                }else{
                                    io:println(result);
                                }    
           
       }else if (heyy === "3") {
           io:println("****READ WITH RECORD KEY****");
           string read_key = io:readln("Enter Record Key :");
           var result = blockingEp->readRecordWithKey({"RecordKey": read_key
                                    });
                                     
             //RETURN RESPONSE
                 if(result is grpc:Error){
                                            io:println(result.reason());
                                }else{
                                    io:println(result);
                                }     
                        
       }else if (heyy === "4") {
           //read record with kkeyn
            io:println("****READ WITH RECORD KEY AND VERSION****");
           string read_key = io:readln("Enter Record Key :");
            string read_version = io:readln("Enter Record Key :");
            int|error res1 = 'int:fromString(read_version);

           var result = blockingEp->readRecordWithKeyVer({"RecordKey": read_key,
                                      "RecordVersion": <int>res1
                                   });
        //RETURN RESPONSE
                 if(result is grpc:Error){
                                            io:println(result.reason());
                                }else{
                                    io:println(result);
                                }     

       }else if (heyy === "5") {
            io:println("****READ WITH RECORD CRITERION ****");

            string songtitle = io:readln("Enter Song Title :");
            string artistname = io:readln("Enter Artist Name :");
            string bandName = io:readln("Enter Band Name :");
           
               var result = blockingEp->readRecordWithCriterion({"songtitle": songtitle,
                                      "artistname": artistname,
                                      "bandName":bandName
                                    });
                    if(result is grpc:Error){
                                            io:println(result.reason());
                                }else{
                                    io:println(result);
                                }    
           
           
       }

    
                                
                               

}


