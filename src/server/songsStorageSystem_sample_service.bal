import ballerina/crypto;
import ballerina/grpc;
import ballerina/io;

import ballerina/log;
import ballerina/file;

//import ballerina/filepath;

listener grpc:Listener ep = new (9090);



//function sorting(json|error a, json|error b) {
 //   return a.RecordVersion - b.RecordVersion;
//}
type OddNumberGenerator object {
    int i = 1;
    
    public function next() returns record {|int value;|}|error? {
        self.i += 2;
        return {value: self.i};
    }
};

//READ AND WRITE TO FILE 
function closeRc(io:ReadableCharacterChannel rc) {
    var result = rc.close();
    if (result is error) {
        log:printError("Error occurred while closing character stream",
                        err = result);
    }
}

function closeWc(io:WritableCharacterChannel wc) {
    var result = wc.close();
    if (result is error) {
        log:printError("Error occurred while closing character stream",
                        err = result);
    }
}

//writes and updates json files
function write_into_json(json content, string path) returns @tainted error? {

    io:WritableByteChannel wbc = check io:openWritableFile(path);

    io:WritableCharacterChannel wch = new (wbc, "UTF8");
    var result = wch.writeJson(content);
    closeWc(wch);
    return result;
}
//read json 
function read_from_json(string path) returns @tainted json|error {

    io:ReadableByteChannel rbc = check io:openReadableFile(path);

    io:ReadableCharacterChannel rch = new (rbc, "UTF8");
    var result = rch.readJson();
    closeRc(rch);
    return result;
}
//THE END 
service songsStorageSystem on ep {
    //inserting 
    resource function insertRec(grpc:Caller caller, Record value) {
        // Implementation goes here.
        // --------------------- hashing function -------------****
       
       string input = value.toString();
       byte[] inputArr = input.toBytes();
       //encrypt to md5
        byte[] recordKey = crypto:hashMd5(inputArr);
        // --------------------- hashing ending----------------****

        //convert record into a json
       map<json>|error record_json = map<json>.constructFrom(value);
        //check if the above convertion has an  error
        if (record_json is error) {

           io:println("Error");
       } else {
            //attaching the record key
           record_json["RecordKey"] = recordKey.toBase16();
           //create a file path with the record key as filename
           string filePath = "./files/" +recordKey.toBase16()+  ".json";

           // --------------------- write to JSON File----------------****
           
           //check whether file exists  in the directory
           boolean fileExists = file:exists(filePath);
            
                
            map<json>  record_results;
            
            if(fileExists.toString() === "true"){
            //read json file 
           map<json>  rResult =  <map<json>> read_from_json(filePath);
           // return the latest record key and version if file exists
                record_results=  {RecordKey: rResult["RecordKey"], RecordVersion: rResult["RecordVersion"]};
            }else{
               
                 //write to a json file 
                 var wResult = write_into_json(record_json, filePath);
                  //if file does not exist return record key and version 1 which is the initial

                   record_results = {RecordKey: recordKey.toBase16(), RecordVersion: 1};

            }

         //grpc:Error? result = caller->send();s
           
          grpc:Error? result = caller->send(record_results);
           if (result is grpc:Error) {
               result = caller->sendError(404, "Page Not Found");
           }
           result = caller->complete();
 
        }

    }
    //updating
    resource function updatingExistingRecord(grpc:Caller caller, updateRecord value) {
        // Implementation goes here.
        string rResult;
        string input = value.toString();
        byte[] inputArr = input.toBytes();
        byte[] recordKey = crypto:hashMd5(inputArr);

        // --------------------- hashing ending----------------****

        //convert record into a json
        map<json>|error doc = map<json>.constructFrom(value.the_record);
         map<json> record_json = <map<json>>doc;
        //check if the above convertion- is error
        if (doc is error) {
           io:println("Error");
        } else {
            
            //inserting record in json object
           
          string record_key_file=<string>value.RecordKey;
         string filePath = "./files/" + record_key_file+  ".json";
         

            boolean fileExists = file:exists(<@untained>  filePath);
           // io:println(fileExists);
        //     //read from json and change the record version
         map<json> rResults =  <map<json>> read_from_json(<@untainted>filePath);
           // io:println(rResults);
           //update the  existing version by  adding 1 
          record_json["RecordVersion"]=(<int>record_json["RecordVersion"]+1);
        if(fileExists.toString() === "true"){
                 var wResult = write_into_json(record_json,<@untained> filePath);
                rResult="record updated succesfully";
               
        }else{
                rResult="record does not exists";
           }
        
           
   

          grpc:Error? result = caller->send(rResult);
          if (result is grpc:Error) {
               result = caller->sendError(404, "Page Not Found");
           }
          result = caller->complete();
        
        
        }
    // You should return a Confirmation
    }
    resource function readRecordWithKey(grpc:Caller caller, readRecordKey value) {
   

        //convert record into a json
        map<json>|error record_json = map<json>.constructFrom(value);
        //check if the above convertion- is error
        if (record_json is error) {
            io:println("Error");
        } else {
            //inserting record in json object
           string filePaths=<string> value.RecordKey.toString();
           //attach the record  with the inputted record key
            string filePath = <string>  "./files/" +value.RecordKey +   ".json";
           //check if file exists
             boolean fileExists = file:exists(<@untained> filePath);
             string rResult; 
             if(fileExists.toString() === "true"){
                 //read existing file
               var heyy = read_from_json(<@untained> filePath).toString();
               //return results
                 rResult="record already exists => " + heyy;
             }else{
                 //return record does not exists if the record is not present
                    rResult ="record does not exists";
             }
           
            //write into file
 
            grpc:Error? result = caller->send(rResult);
            if (result is grpc:Error) {
                result = caller->sendError(404, "Page Not Found");
            }
            result = caller->complete();
        
        }
    // You should return a Record
    }
    resource function readRecordWithKeyVer(grpc:Caller caller, savedResponse value) {
    // Implementation goes here.
          string filePath =  "./files";
    string RecordVersion=value.RecordVersion.toString();
    string RecordKey=value.RecordKey.toString();
    
    //read value from client
    map<json>|error record_criterion = map<json>.constructFrom(value);

    //reading all the records in files directory by name
    file:FileInfo[] readDirResults =checkpanic   file:readDir("./files");
    string heyy =<string>readDirResults.toString();
    
    string[] a = [];
    string results="";
    int i=0;
   
      grpc:Error? err;
      //*****FOR EACH TO LOOP THROUGH THE RETURNED DIRECTORY NAMES****
     foreach var file in readDirResults {
         // GET THE FILE NAME
          string record_path =  "./files/"+file.getName();
         //READ DIRECTORIES
          map<json> all_file_results =  <map<json>> read_from_json(<@untained> record_path);
         //CHECK WHETHER THE RECORD KKEY AND THE RECORD VERSION EXISTS
          if ( RecordVersion===all_file_results["RecordVersion"] && RecordKey===all_file_results["RecordKey"]){
                    //SEND THE RESULTS
                         err = caller->send(all_file_results.toString());
                            if (err is grpc:Error) {
                               // log:printError("Error from Connector: " + err.reason() + " - "
                                //                        + <string>err.detail()["message"]);
                            } else {
                              //  log:printInfo("Send reply: " + all_file_results.toString());
                            }
                continue;
          }
    }

            err = caller->complete();
        
    }


   //@grpc:ResourceConfig {streaming: true}
    
    resource function readRecordWithCriterion(grpc:Caller caller, readWithCriterion value) {
    // Implementation goes here.

    string filePath =  "./files";
    //***** INITIALIZING VARIABLES
    string songtitle="";
    string artistname="";
    string bandName="";
    //CONVERT VALUE TO JSON 
    map<json>|error record_criterion = map<json>.constructFrom(value);

      if (record_criterion is error) {
           io:println("Error");
        } else {
            //GET USER INPUT
            songtitle=<string>record_criterion["songtitle"];
            artistname=<string>record_criterion["artistname"];
            bandName=<string>record_criterion["bandName"];
        }
    //READING ALL THE RECORDS BY NAME
    file:FileInfo[] readDirResults =checkpanic   file:readDir("./files");
    string heyy =<string>readDirResults.toString();
    
    string[] a = [];
    string results="";
    int i=0;
     Record[] recordList = [];
      grpc:Error? err;
      //LOOP THROUGH THE DIRECTORY NAMES 
     foreach var file in readDirResults {
        //GET THE FILE NAMES INDIVIDUALLY FROM THE ARRAY
          string record_path =  "./files/"+file.getName();
        //READ THE RECORD FROM THE FILE
          map<json> all_file_results =  <map<json>> read_from_json(<@untained> record_path);
          //GET THE SONG ARRAY
           json[] songs = <json[]>all_file_results["songs"];
           //GET THE VALUE FROM THE SONGS ARRAY
            map<json> song_info = <map<json>>songs[i];
          //GET THE ARTIST ARRAY
            json[] artist = <json[]>all_file_results["artist"];
        //GET THE ARTIST INFO
             map<json> artist_info = <map<json>>artist[i];
        
             //CHEC 
          if ( song_info["title"]===songtitle){
                    // Record|error inception = Record.constructFrom(all_file_results);
                    // if (inception is Record) {
                    //     recordList.push(inception);
                    //     }
                         err = caller->send(all_file_results.toString());
                            if (err is grpc:Error) {
                               // log:printError("Error from Connector: " + err.reason() + " - "
                                //                        + <string>err.detail()["message"]);
                            } else {
                              //  log:printInfo("Send reply: " + all_file_results.toString());
                            }
                continue;
          }else if(artist_info["name"]===artistname){
            //     Record|error inception = Record.constructFrom(all_file_results);
            //    if (inception is Record) {
            //      recordList.push(inception);
            //     }
                     err = caller->send(all_file_results.toString());
                            if (err is grpc:Error) {
                                // log:printError("Error from Connector: " + err.reason() + " - "
                                //                         + <string>err.detail()["message"]);
                            } else {
                               // log:printInfo("Send reply: " + all_file_results.toString());
                            }
                            
                continue;
          }else if(all_file_results["band"]===bandName){
            //    Record|error inception = Record.constructFrom(all_file_results);
            //    if (inception is Record) {
            //      recordList.push(inception);
            //     }
                err = caller->send(all_file_results.toString());
                            if (err is grpc:Error) {
                                // log:printError("Error from Connector: " + err.reason() + " - "
                                //                         + <string>err.detail()["message"]);
                            } else {
                                // log:printInfo("Send reply: " + all_file_results.toString());
                            }

                continue;
          }
          
       i=i+1;//incrementing array
        // grpc:Error? result = caller->send(recordList);
        //   if (result is grpc:Error) {
        //         result = caller->sendError(404, "Page Not Found");
        //     }
          
         // result = caller->complete();   
    // You should return a AllRecords
    }

            err = caller->complete();
        
    }
    
   
     
}

public type Record record {|
    string name = "";
    string date = "";
    string band = "";
    Song[] songs = [];
    Artist[] artist = [];
    string RecordKey = "";
    int RecordVersion = 0;

|};

public type Song record {|
    string title = "";
    string genre = "";
    string platform = "";

|};


public type Artist record {|
    string name = "";
    Member? member = ();

|};

public type Member "YES"|"NO";
public const Member MEMBER_YES = "YES";
const Member MEMBER_NO = "NO";


public type savedResponse record {|
    string RecordKey = "";
    int RecordVersion = 0;

|};

public type updateRecord record {|
    string RecordKey = "";
    int RecordVersion = 0;
    Record? the_record = ();
    

|};

public type Confirmation record {|
    string confirm = "";

|};

public type readRecordKey record {|
    string RecordKey = "";

|};

public type readWithCriterion record {|
    string songtitle = "";
    string artistname = "";
    string bandName = "";

|};

public type AllRecords record {|
    //Record[] records = [];
      string RecordKey = "";
|};



const string ROOT_DESCRIPTOR = "0A0A63616C692E70726F746F228C030A065265636F726412120A046E616D6518012001280952046E616D6512120A046461746518022001280952046461746512120A0462616E64180320012809520462616E6412220A05736F6E677318042003280B320C2E5265636F72642E536F6E675205736F6E677312260A0661727469737418052003280B320E2E5265636F72642E4172746973745206617274697374121C0A095265636F72644B657918062001280952095265636F72644B657912240A0D5265636F726456657273696F6E180720012805520D5265636F726456657273696F6E1A4E0A04536F6E6712140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D1A660A0641727469737412120A046E616D6518012001280952046E616D65122D0A066D656D62657218022001280E32152E5265636F72642E4172746973742E4D656D62657252066D656D62657222190A064D656D62657212070A03594553100012060A024E4F100122530A0D7361766564526573706F6E7365121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E227A0A0C7570646174655265636F7264121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E12260A0A7468655F7265636F726418032001280B32072E5265636F726452097468655265636F726422280A0C436F6E6669726D6174696F6E12180A07636F6E6669726D1801200128095207636F6E6669726D222D0A0D726561645265636F72644B6579121C0A095265636F72644B657918012001280952095265636F72644B6579226D0A117265616457697468437269746572696F6E121C0A09736F6E677469746C651801200128095209736F6E677469746C65121E0A0A6172746973746E616D65180220012809520A6172746973746E616D65121A0A0862616E644E616D65180320012809520862616E644E616D65222F0A0A416C6C5265636F72647312210A077265636F72647318012003280B32072E5265636F726452077265636F726473328D020A12736F6E677353746F7261676553797374656D12240A09696E7365727452656312072E5265636F72641A0E2E7361766564526573706F6E736512360A167570646174696E674578697374696E675265636F7264120D2E7570646174655265636F72641A0D2E436F6E6669726D6174696F6E122C0A11726561645265636F7264576974684B6579120E2E726561645265636F72644B65791A072E5265636F7264122F0A14726561645265636F7264576974684B6579566572120E2E7361766564526573706F6E73651A072E5265636F7264123A0A17726561645265636F726457697468437269746572696F6E12122E7265616457697468437269746572696F6E1A0B2E416C6C5265636F726473620670726F746F33";
function getDescriptorMap() returns map<string> {
    return {
        "cali.proto": "0A0A63616C692E70726F746F228C030A065265636F726412120A046E616D6518012001280952046E616D6512120A046461746518022001280952046461746512120A0462616E64180320012809520462616E6412220A05736F6E677318042003280B320C2E5265636F72642E536F6E675205736F6E677312260A0661727469737418052003280B320E2E5265636F72642E4172746973745206617274697374121C0A095265636F72644B657918062001280952095265636F72644B657912240A0D5265636F726456657273696F6E180720012805520D5265636F726456657273696F6E1A4E0A04536F6E6712140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D1A660A0641727469737412120A046E616D6518012001280952046E616D65122D0A066D656D62657218022001280E32152E5265636F72642E4172746973742E4D656D62657252066D656D62657222190A064D656D62657212070A03594553100012060A024E4F100122530A0D7361766564526573706F6E7365121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E227A0A0C7570646174655265636F7264121C0A095265636F72644B657918012001280952095265636F72644B657912240A0D5265636F726456657273696F6E180220012805520D5265636F726456657273696F6E12260A0A7468655F7265636F726418032001280B32072E5265636F726452097468655265636F726422280A0C436F6E6669726D6174696F6E12180A07636F6E6669726D1801200128095207636F6E6669726D222D0A0D726561645265636F72644B6579121C0A095265636F72644B657918012001280952095265636F72644B6579226D0A117265616457697468437269746572696F6E121C0A09736F6E677469746C651801200128095209736F6E677469746C65121E0A0A6172746973746E616D65180220012809520A6172746973746E616D65121A0A0862616E644E616D65180320012809520862616E644E616D65222F0A0A416C6C5265636F72647312210A077265636F72647318012003280B32072E5265636F726452077265636F726473328D020A12736F6E677353746F7261676553797374656D12240A09696E7365727452656312072E5265636F72641A0E2E7361766564526573706F6E736512360A167570646174696E674578697374696E675265636F7264120D2E7570646174655265636F72641A0D2E436F6E6669726D6174696F6E122C0A11726561645265636F7264576974684B6579120E2E726561645265636F72644B65791A072E5265636F7264122F0A14726561645265636F7264576974684B6579566572120E2E7361766564526573706F6E73651A072E5265636F7264123A0A17726561645265636F726457697468437269746572696F6E12122E7265616457697468437269746572696F6E1A0B2E416C6C5265636F726473620670726F746F33"

    };
}

